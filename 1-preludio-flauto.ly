\version "2.19.36"

preludioFlauto = {
 
 \relative f'' {
  
  R1*2
  f1 ~ f8 g ees d bes a g c
  d a'( c g) ~ g f( a e)
  d( ees! c4) ~ c8( d bes4) ~
  bes a a8( g) fis4
  f!8 f'! f( ees) ees( des) des( c)
  
  b2 ~ b8 g'( c des
  bes2) ~ bes8 c( a bes)
  aes2( bes8) bes( g aes)
  ges2( aes8) aes( f ges)
  fes( aes c! ees) g!( f!) f( ees)
  c4 r r2
  c,8[ a'] bes,[ g'] a,[ fis'] fis([ g]
  a bes c a) bes2 ~
  bes8( a) g4 ~ g8 a( g a)
  d,1
  d'8( c b a) gis4 fis
  g!8( f! e! d) c4 bes!
  
  \acciaccatura { a8 } g4 b! e2
  \acciaccatura { d8 } cis4 e! a2
  g8( d' f c) ~ c bes( d a)
  bes( aes ges) aes( f4) ees!
  d1 ~
  d4 fis8([ cis)] cis([ d)] r4
  r8 a'( fis[ e] c![ a] g[ e)]
  cis([ d] a'[ g] fis4) g8([ a]
  
  b c d4) r8 c d,([ e]
  f[ g] a4) r8 b c,[( d]
  \time 3/2 e[ fis] g[ a)] d([ c)] b([ a)] a b4 dis8
  \time 4/4 \acciaccatura { dis,8 } e4 r e'! g
  bes8([ a)] a([ g)] cis,4 d
  c'!8([ bes)] bes([ a)] gis[ c(] a[ g)]
  fis g4 a c, d8
  ees( d) d4\trill c f( ~
  
  f8 g d c) d8.( e16 fis8. g16)
  c4. e4( d8 a[ b)] ~
  b c( g a) ~ a b( fis g) ~
  g4 a8( g) ~ g( f) f( ees)
  d4 d'2( ~ d8 c)
  a( bes d ees) fis,4. g8
  aes( f) f( g) d( f) f( ees)
  f( c) c( ees) bes( d) d( c)
  \time 3/2 a4.( bes8) g4.( aes8) fes( aes des fes)
  \time 4/4
  g!4. a8 g( f e f)
  g4 a8( b c d e f
  g4) f4.( ees8) ees([ des)]
  
  c( bes a bes g) a4 bes!8(
  fis4) r r2
  c2.( ~ c8 f)
  d( c b d) ees, c' d, bes'
  c, a'! a( bes c d ees c
  d) a bes2 bes8( a
  g f) ees4 g bes8( c
  d4) r r2
  
  R1*2
  a'1 ~
  a8 bes g f d c bes ees
  f c'( ees bes) ~ bes( a c g) ~
  g4 a8( bes a g f g)
  \time 3/2 a8( g) g( f!) f( ees) ees( d) d( c) c( bes)
  \time 4/4 bes( a) a( g) g( fis) fis4 ~
  fis2 r4 ees'4
  \time 3/2 g8 des( c bes) g' c,( bes a) g' bes,( a aes)
  fis g( ees d) c' a( g fis!) ~ fis( a c fis) ~
  fis4. g8 g,1
  g1. \bar "|."
  
 }
 
}