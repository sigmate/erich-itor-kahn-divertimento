\version "2.19.36"

vTitle = "Divertimento"
vSubtitle = "per il Flauto e Violino con accompagnamento del Cembalo"
vSubsubtitle = "composé en 1927"
vComposer = "Erich Itor Kahn"