\version "2.19.36"

preludioViolino = {
 
 \relative bes' {
  
  R1*4
  bes1 ~
  bes8 c a g ees d c f
  g d'( f c) ~ c bes( d a)
  aes( bes g4) ~ g8( aes f4) ~
  
  f8 g( e f ees2) ~
  ees8 bes'( f' g e2)
  f8 f( d ees) des2(
  ees8) ees( c des ces2)
  des8( fes aes c!) ees( d!) d( c)
  b( c d a!) g( f) g d
  ees2.( ~ ees8 ees)
  d( c bes a)
  g[ ees'] f,[ d']
  e, cis' cis( d e f g e)
  fis1(
  gis4) gis,8( a b d cis b)
  bes4 c,!8( d ees g f ees)
  
  d( b' a g) f( a d c)
  b( a g a) bes( ees! g f) 
  ees2 d
  des bes,8( des ges bes)
  b4 a ~ a8 g g fis
  eis4 d''8( ais) ais( b) r4
  r8 c!( a g) a[( e]) e[( cis])
  r4 dis8( e d'! b fis'4) ~
  
  fis8 e a( g e[ d] b[ a])
  r b f'!( e c b g fis)
  \time 3/2 r e d'[( c] a2) ~ a4 g
  \time 4/4 e2 r
  cis,4 e a8[( g]) g[( f])
  e4 d'2( cis4)
  c!8[( b]) b[( a]) g[( f] ees[ d])
  c4 d8( ees f g) g4(\trill
  aes) des, fis8( b a g)
  a( e'! a g fis) f4( d8)
  ees4.( c8) cis4.( a8)
  bes8( ees,) g( c,) ees( a,) g'( fis)
  a( d) d( c) c( d) d( a)
  c( bes) bes( c) c( a) a( bes)
  b4 ees,4.-- d8( bes' g)
  a4.( bes,8) g'4 f8 e ~
  \time 3/2 e bes( ees d ees) aes,( des c des4) ces
  \time 4/4 a8( e' c a) b( a b d)
  a( e' c a) d( c e a)
  bes( f' des bes) ees( ges) des( f)
  
  e4.( c8) ees4( d8 c)
  d( c) c( d) a( c) c( bes)
  aes[ f'] g,[ ees'] f,[ d'] d( ees
  f g aes f) g2 ~
  g4. g8 ees'( d c bes
  a4) g8( f ees d c4)
  ees d8( c bes a) f( ees
  d4) r r2
  
  f'1 ~
  f8 g ees d bes a g c
  d ees,( g c) ees( d) d( c)
  bes( g) c,( ees f d) g4 ~
  g a8( bes c d ees c
  a) ees'( g d) ~ d c( ees bes)
  \time 3/2 fis( a) a( d) d( d,) d( f) f( a) a( c,)
  \time 4/4 c( cis) e( a,) a( bes) bes( c)
  d( c) ees4 r2
  \time 3/2 r1 g,2
  
  ees'8 d( c bes)ees c( bes a) ees' c( a g) ~
  g( d') d( c) c( d) d( a) g-- a-- d-- g-- ~
  g1.
  
  
 }
 
}