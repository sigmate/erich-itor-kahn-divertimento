\version "2.19.36"

preludioCembaloRight = {
 
  \relative g' {
  
    <<
    
      \new Voice {
      
        \voiceOne
      
        g8 d' bes g a g a c
        g d' bes g a g a c
        g! d' bes g a g bes c
        d c bes a d f, ees! d
      
      
      }
    
      \new Voice {
      
        \voiceTwo
      
        d2 ees4 ees ~
        ees c f ges8 f
        e!4 f e d ~
        d2 ~ d8 f ees d
      
      }
    
    >>
  
    \voiceOne d c \change Staff = "cembaloLeft" \voiceOne c bes bes a a g
    fis d \change Staff = "cembaloRight" d' \change Staff = "cembaloLeft" a \change Staff = "cembaloRight" bes g ees'4
    d8 c d ees f g a c
    c d ees g aes bes c des
    d c b a g f ees f
    g f ees! d g bes, a g
    f g aes g aes bes c aes
    des c bes aes ges f ees aes ~
    aes?4. bes8 aes g aes g
    f ees a c ees c b a
    g f g ees g d g c,
    a' g a f bes c d g
    g, a bes e cis d e a
    fis g a fis d' c b a
    gis a gis fis eis b a gis
    g! d fis a ees d c bes!
    a c f e c' b b a
    gis cis fis e d c bes a
    a g a f g f g ees
    f e f des ees bes c des
    d e fis g a b cis d
    b a gis4 r <d fis b>
    <e g c!> r r <e g cis ~>
    <d fis cis'>2 <d fis bes>
    
    <c e b'> <c e a>
    <b d a'> <b d g>
    \time 3/2
    <<
      
      \new Voice {
        \voiceOne
       
         g'4 fis e2 dis
       
      }
      
      \new Voice {
        \voiceTwo
        
        <a c>2 <a b>1
        
      }
      
    >>
    \time 4/4
    <g b e>2 c'8 b b a
    g f e d e f g a
    bes a g f fis gis a a
    bes g d' g f ees g c,
    c bes c bes a4 r
    
    r b8 a b cis d e
    e fis g a b a b b,
    c d ees f g fis a d,
    ees! c f a, g a bes a
    g4 fis r2
    g'4 f! ees d ~
    d8 ees c ees g,4. a8
    ees f g4 ~ g8 bes d e
    \time 3/2 f8 d'4 ees, c'8 aes des, bes' aes ges fes
    \time 4/4 d!1\startTrillSpan ~
    d2 \appoggiatura { c16\stopTrillSpan d } e8 f d des
    c d! e f g d' f c
    
    bes g d bes g ees' b a
    aes4 g g fis
    f!8 aes c ees d aes' c g ~
    g f ees f d ees f ees ~
    ees d c d ees c fis, g
    a f! d c c bes a4
    ees'8 d c4 d8 c bes4
    bes'8 a g2 bes8 a
    
    g d' bes g a g a c
    g d' bes g f c d ees
    d c ees g c ees g bes
    d c ees bes bes a c g
    a g fis f ees d c bes
    a c ees f g a, bes c
    \time 3/2 d c c d d a! a c c bes bes c
    \time 4/4 c g g bes bes des des a
    f! ees c bes a4 r
    \time 3/2 <des g bes>2 <c fis bes> <ces f bes>
    
    <bes ees g bes> <d fis a> <c ees bes>
    <bes ees g bes> <d f bes> <c ees bes'>
    <bes d bes'>1. \bar "|."
  
  
  }
 
}