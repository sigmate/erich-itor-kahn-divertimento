\version "2.19.36"

preludioCembaloLeft = {
 
 \relative g, {
  
    g4 g' f f,
    
    ees ees' d des,
    c c' bes bes,
    a a' g g,
    \voiceTwo
    f b d f
    fis g \voiceOne a8 bes c d
    ees4 f8 ees d2 ~
    d4 c bes aes
    g g' a! a,!
    bes bes' c c,
    f, f' bes bes,
    ees, ees' aes aes,
    des, f aes c
    g2 g' ~
    g ~ g8 a bes4
    f c'8 d ees4 ees,
    e4. f8 g e cis'4
    d,8[ bes'] c,[ a'] b,[ gis'] gis[ a]
    b c d b cis,2
    fis8 g a fis a,4 g
    f! e d d'
    fis bes, ees g
    c f, bes ees,
    aes des, ges ges,(
    fis8) b d fis b4. g8
    gis a b4 r b,
    a r r a
    d,2 g
    c, fis
    b, e
    \time 3/2 a, b1
    \time 4/4 e2 r
    e'8 f e a bes4 a
    g8 a bes c d4 f,
    ees' e, a bes,
    ees c f f'8 ees
    d4 gis,2 cis4
    fis, b8 cis d4 g, ~
    g f e d
    des4. c8 bes c d4 ~
    d2 d,4 ees'' ~
    ees4. d8 bes c ees f
    g,4 aes a c,
    f8 ees d c d c bes c
    \time 3/2 des4 g, ces fes, ges8 fes ees des
    \time 4/4 b4 a g a
    b a g a
    r g' aes bes
    c8 d e4 f fes
    ees8[ c'] d,[ bes'] c, a' a bes
    c d ees c bes4. a8
    b4 g aes4. g8
    f4. ees8 d4 a
    bes ees, f8 g a bes
    c4 c,8 d ees f g bes
    d d' d c c d d a
    g4 g, f f'
    ees ees, d d'
    c c, bes bes'8 c
    d ees f4 g8 a bes c
    des4 c bes a
    g g' ~ g8 f ees4
    \time 3/2 d bes aes c bes g ~
    \time 4/4 g cis, c fis, ~
    fis8 g a bes c4 r
    \time 3/2 f2 ees d
    c bes a
    g4 bes ees c fis2
    g1. \bar "|."
  
 }
 
}