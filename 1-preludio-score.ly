\version "2.19.36"

\include "common.ly"
\include "1-preludio-flauto.ly"
\include "1-preludio-violino.ly"
\include "1-preludio-cembalo-right.ly"
\include "1-preludio-cembalo-left.ly"

\header {
  
  title = \vTitle
  subtitle = \vSubtitle
  subsubtitle = \vSubsubtitle
  composer = \vComposer
  
}

\header {
  
  piece = "I Preludio"
  
}

\score {
  
  \new StaffGroup <<
    
    \new Staff \with {
      
      instrumentName = "Flauto"
      midiInstrument = "flute"
      
    }
    {
     
     \clef treble
     \key g \minor
     \preludioFlauto
     
    }

    \new Staff \with {
      
      instrumentName = "Violino"
      midiInstrument = "violin"
      
    }
    {
      
      \clef treble
      \key g \minor
      \preludioViolino
      
    }
    
    \new PianoStaff \with
    {
     
       instrumentName = "Cembalo"
       midiInstrument = "acoustic grand"
     
    }
    <<
      
      \new Staff = "cembaloRight" {
        
        \clef treble
        \key g \minor
        \preludioCembaloRight
        
      }
      
      \new Staff = "cembaloLeft" {
        
        \clef bass
        \key g \minor
        \preludioCembaloLeft
        
      }
      
    >>

  >>
  
  \layout {}
  \midi {}
  
}